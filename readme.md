# Room Booking Solution

This is a simple room booking solution built with ASP.NET.

## Features

All features are tracked as enhancements in this project's Issues list. Here is a high level list of features by area -

### Web site

This application is available to most human users as a website that provides the following functionality

* View list of rooms
* View availability of rooms
* Book a room
* Find a room based on criteria

### API

This application provides a REST API that has the following endpoints

* **GET /api/rooms** - Get the list of rooms available for booking
* **GET /api/rooms/{room name}/bookings/{year}/{month}/{day}** - Get all the bookings of the specified date of a room
* **POST /api/rooms/{room name}/bookings/{year}/{month}/{day}** - Make a booking for the specified room at the date. Accepts start time, number of attendees and duration in body.

## Architecture

The application is split into 3 layers -

### Web Client

The user interface is built using [ASP.NET MVC](http://www.asp.net/mvc/mvc5) and [Angular.js](https://angularjs.org/). The MVC controllers simply serve the HTML that bootstraps the Angular Single Page Application (SPA) component. Angular scripts (modules/controllers/services/etc.) are bundled and served by ASP.NET. Angular views are served as HTML files from the the file system.

#### Angular folder structure

* Scripts/
    * home/
        * app.js
        * controllers/
            * *Ctrl.js 
        * services/
            * *Service.js
        * specs/
            * controllers/
                * *Ctrl.Spec.js
            * services/
                * *Service.Spec.js
        * views/
            * *.html

The above folder structure allows us to keep all related files close to eachother, thus making it easier to navigate the solution.

Specs are written using the Jasmine BDD testing framework. These are not served to the application, but are part of our solution so should be treated as production code.

#### ASP.NET MVC folder structure

The structure of the Web project is typical for ASP.NET MVC applications.

* Web/
    * App_Start/
        * BundleConfig.cs - Configure bundling and optimisation of static resources (scripts/css)
        * DatabaseConfig.cs - Configure data store, ensuring it is in the correct state for the current version of the application.
        * DependencyConfig.cs - Configure IoC container to inject dependencies into controllers and register any modules
        * etc.
    * Content/
        * *.css
    * Controllers/
        * Api/
            * API Controllers (serve JSON/XML)
        * MVC Controllers (serve HTML)
    * Core/
        * Module.cs - configure custom dependencies to be injected
    * Models/
        * Api/
            * Models used for API Controller requests/responses
        * Models used for MVC Controller requests/responses
    * Data/
        * Models used by data store
        * Helpers/Extensions for querying data store
    * Scripts/
        * home - Angular application
        * i18n - Angular internationalisation scripts
        * angular* - Angular framework compoments/modules
        * bootstrap.js - JavaScript components provided by Bootstrap framework
        * jquery* - jQuery libraries
        * modernizr* - Modernizr library for detecting browser features
        * moment* - Moment.js framework
        * etc. (frameworks/libraries)
    * Views/
        * Views renders by MVC Controllers

### REST API

As per the ASP.NET MVC folder structure above the REST API is provided by [ASP.NET WebAPI](http://www.asp.net/web-api), which is included in the MVC application. The controllers and models are under **Api** subdirectories in the appropriate MVC folders.

Routing is performed using both the default routing mechanism declared in **App_Start/WebApiConfig.cs** and using [Attribute Routing](http://blogs.msdn.com/b/webdev/archive/2013/10/17/attribute-routing-in-asp-net-mvc-5.aspx) to provide declarative non-standard routes.

### Database

Data storage is accomplised using [MongoDB](https://www.mongodb.org/). This allows for a flexible data model that can be improved and refined over the iterative software development process.

Communication with the data store is accomplished via the [MongoRepository](https://github.com/RobThree/MongoRepository) library, which provides a simply abstraction over [Mongo C# Driver](https://github.com/mongodb/mongo-csharp-driver) for querying and executing commands against the database. As mentioned above, any configuration of the data store occurs in **App_Start/DatabaseConfig.cs**. Repositories are defined in **Core/Module.cs** to be injected into dependent objects.

## Open Source Libraries

### HTML/CSS/JavaScript

* **[Bootstrap](http://getbootstrap.com)** - framework providing common HTML elements and CSS components allowing you to quickly and easily build responsive web experiences.
* **[AngularJS](https://angularjs.org)** - framework for buildings single page applications using MV* patterns
* **[Moment.js](http://momentjs.com)** - date parsing and formatting library

### .NET

* **[Autofac](http://autofac.org)** - IoC container
* **[Elmah](https://code.google.com/p/elmah/)** - Error reporting and logging
* **[Mongo C# Driver](https://github.com/mongodb/mongo-csharp-driver)** - .NET driver for communicating with MongoDB
* **[MongoRepository](https://github.com/RobThree/MongoRepository)** - Wrapper for Mongo C# Driver providing a simple repository pattern

