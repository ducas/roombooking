# Contributing

## Requirements

* Visual Studio 2015
* MongoDB
* An internet connection (to download NuGet packages)

## Mantra

* Feature branch all the things...
* Use issue numbers in commit messages
* Any reference data is initialised in App_Start/DatabaseConfig.cs
* Unit tests are gold!
