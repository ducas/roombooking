﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using NUnit.Framework;
using RoomBooking.Data;
using Booking = RoomBooking.Data.RoomBooking;

namespace RoomBooking.Tests.Data.RoomBookingQueryable
{
    public class AvailableSlots
    {
        private static readonly DateTime Start = new DateTime(2015, 1, 1, 9, 0, 0);
            private static readonly DateTime End = new DateTime(2015, 1, 2);
        public class should_return_slot_from_start_to_end_of_day
        {

            [Test]
            public void when_no_bookings()
            {
                var bookings = new List<Booking>().AsQueryable();

                var results = bookings.AvailableSlots(Start, End);

                results.ShouldBeEquivalentTo(new[] { new Booking { StartTime = Start, EndTime = Start.Date.AddDays(1) } });
            }
        }

        public class should_return_no_slots
        {
            [Test]
            public void when_booked_entire_day_by_one_booking()
            {
                var bookings = new List<Booking>
                {
                    new Booking { StartTime = new DateTime(2015,1,1,0,0,0,DateTimeKind.Local), EndTime = new DateTime(2015,1,2,0,0,0,DateTimeKind.Local) }
                }.AsQueryable();

                var results = bookings.AvailableSlots(Start, End);

                results.ShouldBeEquivalentTo(new Booking[0]);
            }

            [Test]
            public void when_booked_entire_day_by_multipls_bookings()
            {
                var bookings = new List<Booking>
                {
                    new Booking { StartTime = new DateTime(2015, 1, 1, 0, 0, 0,DateTimeKind.Local), EndTime = new DateTime(2015, 1, 1, 9, 0, 0,DateTimeKind.Local) },
                    new Booking { StartTime = new DateTime(2015, 1, 1, 9, 0, 0,DateTimeKind.Local), EndTime = new DateTime(2015, 1, 1, 15, 0, 0,DateTimeKind.Local) },
                    new Booking { StartTime = new DateTime(2015, 1, 1, 15, 0, 0,DateTimeKind.Local), EndTime = new DateTime(2015, 1, 2, 0, 0, 0) }
                }.AsQueryable();

                var results = bookings.AvailableSlots(Start, End);

                results.ShouldBeEquivalentTo(new Booking[0]);
            }
        }

        public class should_return_one_slot
        {
            [Test]
            public void when_one_booking_ends_at_start_time()
            {
                var bookings = new List<Booking>
                {
                    new Booking { StartTime = new DateTime(2015, 1, 1, 0, 0, 0,DateTimeKind.Local),EndTime = new DateTime(2015, 1, 1, 9, 0, 0,DateTimeKind.Local) }
                }.AsQueryable();

                var results = bookings.AvailableSlots(Start, End);

                results.ShouldBeEquivalentTo(new[] { new Booking { StartTime = new DateTime(2015, 1, 1, 9, 0, 0), EndTime = new DateTime(2015, 1, 2, 0, 0, 0) } });
            }

            [Test]
            public void when_one_booking_ends_after_start_time()
            {
                var bookings = new List<Booking>
                {
                    new Booking { StartTime = new DateTime(2015, 1, 1, 0, 0, 0,DateTimeKind.Local),EndTime = new DateTime(2015, 1, 1, 10, 0, 0,DateTimeKind.Local) }
                }.AsQueryable();

                var results = bookings.AvailableSlots(Start, End);

                results.ShouldBeEquivalentTo(new[] { new Booking { StartTime = new DateTime(2015, 1, 1, 10, 0, 0), EndTime = new DateTime(2015, 1, 2, 0, 0, 0) } });
            }

            [Test]
            public void when_one_booking_ends_before_start_time()
            {
                var bookings = new List<Booking>
                {
                    new Booking { StartTime = new DateTime(2015, 1, 1, 0, 0, 0,DateTimeKind.Local),EndTime = new DateTime(2015, 1, 1, 8, 0, 0,DateTimeKind.Local) }
                }.AsQueryable();

                var results = bookings.AvailableSlots(Start, End);

                results.ShouldBeEquivalentTo(new[] { new Booking { StartTime = new DateTime(2015, 1, 1, 9, 0, 0), EndTime = new DateTime(2015, 1, 2, 0, 0, 0) } });
            }
            [Test]
            public void when_one_starts_after_start_time()
            {
                var bookings = new List<Booking>
                {
                    new Booking { StartTime = new DateTime(2015, 1, 1, 12, 0, 0,DateTimeKind.Local),EndTime = new DateTime(2015, 1, 2, 0, 0, 0) }
                }.AsQueryable();

                var results = bookings.AvailableSlots(Start, End);

                results.ShouldBeEquivalentTo(new[] { new Booking { StartTime = new DateTime(2015, 1, 1, 9, 0, 0), EndTime = new DateTime(2015, 1, 1, 12, 0, 0) } });
            }

            [Test]
            public void when_two_bookings_with_gap_between_at_start_time()
            {
                var bookings = new List<Booking>
                {
                    new Booking { StartTime = new DateTime(2015, 1, 1, 0, 0, 0,DateTimeKind.Local),EndTime = new DateTime(2015, 1, 1, 9, 0, 0,DateTimeKind.Local) },
                    new Booking { StartTime = new DateTime(2015, 1, 1, 12, 0, 0,DateTimeKind.Local), EndTime = new DateTime(2015, 1, 2, 0, 0, 0) }
                }.AsQueryable();

                var results = bookings.AvailableSlots(Start, End);

                results.ShouldBeEquivalentTo(new[] { new Booking { StartTime = new DateTime(2015, 1, 1, 9, 0, 0), EndTime = new DateTime(2015, 1, 1, 12, 0, 0) } });
            }

            [Test]
            public void when_two_bookings_with_gap_between_after_start_time()
            {
                var bookings = new List<Booking>
                {
                    new Booking { StartTime = new DateTime(2015, 1, 1, 0, 0, 0,DateTimeKind.Local),EndTime = new DateTime(2015, 1, 1, 10, 0, 0,DateTimeKind.Local) },
                    new Booking { StartTime = new DateTime(2015, 1, 1, 12, 0, 0,DateTimeKind.Local), EndTime = new DateTime(2015, 1, 2, 0, 0, 0) }
                }.AsQueryable();

                var results = bookings.AvailableSlots(Start, End);

                results.ShouldBeEquivalentTo(new[] { new Booking { StartTime = new DateTime(2015, 1, 1, 10, 0, 0), EndTime = new DateTime(2015, 1, 1, 12, 0, 0) } });
            }

            [Test]
            public void when_two_bookings_with_gap_between_before_start_time()
            {
                var bookings = new List<Booking>
                {
                    new Booking { StartTime = new DateTime(2015, 1, 1, 0, 0, 0,DateTimeKind.Local),EndTime = new DateTime(2015, 1, 1, 8, 0, 0,DateTimeKind.Local) },
                    new Booking { StartTime = new DateTime(2015, 1, 1, 12, 0, 0,DateTimeKind.Local), EndTime = new DateTime(2015, 1, 2, 0, 0, 0) }
                }.AsQueryable();

                var results = bookings.AvailableSlots(Start, End);

                results.ShouldBeEquivalentTo(new[] { new Booking { StartTime = new DateTime(2015, 1, 1, 9, 0, 0), EndTime = new DateTime(2015, 1, 1, 12, 0, 0) } });
            }
        }

        public class should_return_multiple_slots
        {
            [Test]
            public void when_bookings_have_gaps_between_at_start_time()
            {
                var bookings = new List<Booking>
                {
                    new Booking { StartTime = new DateTime(2015, 1, 1, 0, 0, 0 ),EndTime = new DateTime(2015, 1, 1, 7, 0, 0) },
                    new Booking { StartTime = new DateTime(2015, 1, 1, 8, 0, 0 ),EndTime = new DateTime(2015, 1, 1, 9, 0, 0) },
                    new Booking { StartTime = new DateTime(2015, 1, 1, 9, 30, 0),EndTime = new DateTime(2015, 1, 1, 10, 0, 0) },
                    new Booking { StartTime = new DateTime(2015, 1, 1, 11, 0, 0), EndTime = new DateTime(2015, 1, 1, 12, 0, 0) },
                    new Booking { StartTime = new DateTime(2015, 1, 1, 13, 0, 0), EndTime = new DateTime(2015, 1, 1, 20, 0, 0) }
                }.AsQueryable();

                var start = new DateTime(2015, 1, 1, 9, 0, 0);
                var results = bookings.AvailableSlots(start, new DateTime(2015, 1, 2));

                results.ShouldBeEquivalentTo(new[]
                {
                    new Booking { StartTime = new DateTime(2015, 1, 1, 9, 0, 0), EndTime = new DateTime(2015, 1, 1, 9, 30, 0) },
                    new Booking { StartTime = new DateTime(2015, 1, 1, 10, 0, 0), EndTime = new DateTime(2015, 1, 1, 11, 0, 0) },
                    new Booking { StartTime = new DateTime(2015, 1, 1, 12, 0, 0), EndTime = new DateTime(2015, 1, 1, 13, 0, 0) },
                    new Booking { StartTime = new DateTime(2015, 1, 1, 20, 0, 0), EndTime = new DateTime(2015, 1, 2, 0, 0, 0) },
                });
            }
        }
    }
}
