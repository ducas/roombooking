﻿using System.Collections.Generic;
using System.Linq;
using MongoRepository;
using NSubstitute;

namespace RoomBooking.Tests
{
    public static class SubstituteExtensions
    {
        public static IRepository<T> AsRepository<T>(this IEnumerable<T> data)
            where T : IEntity<string>
        {
            var mock = Substitute.For<IRepository<T>>();
            var queryable = data.AsQueryable();

            mock.Provider.Returns(queryable.Provider);
            mock.Expression.Returns(queryable.Expression);
            mock.ElementType.Returns(queryable.ElementType);
            mock.GetEnumerator().Returns(queryable.GetEnumerator());

            return mock;
        }
    }
}