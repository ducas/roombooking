﻿using System;
using System.Linq;
using FluentAssertions;
using NUnit.Framework;
using RoomBooking.Controllers.Api;
using RoomBooking.Models.Api;
using Booking = RoomBooking.Data.RoomBooking;
using Room = RoomBooking.Data.Room;

namespace RoomBooking.Tests.Controllers.Api.RoomSearch
{
    public class Get
    {
        public class when_searching_for_rooms
        {
            [Test]
            public void should_get_next_available_slot_for_each_room()
            {
                var rooms = new[]
                {
                    new Room {Name = "Room 1", Capacity = 1, Id = "1"}, // should not be returned (over capacity)
                    new Room {Name = "Room 2", Capacity = 2, Id = "2"}, // should be returned with slot 9 - 10
                    new Room {Name = "Room 3", Capacity = 3, Id = "3"}, // should not be returned (no availability)
                    new Room {Name = "Room 4", Capacity = 4, Id = "4"}, // should be returned with slot 9 - 9:30
                    new Room {Name = "Room 5", Capacity = 5, Id = "5"}, // should be returned with all day available
                    new Room {Name = "Room 6", Capacity = 6, Id = "6" }  // should be returned with slot 10 - 10:30
                }.AsRepository();

                var bookings = new[]
                {
                    new Booking { RoomId = "2", StartTime = new DateTime(2015,1,1), EndTime = new DateTime(2015,1,1,8,0,0) },
                    new Booking { RoomId = "2", StartTime = new DateTime(2015,1,1,10,0,0), EndTime = new DateTime(2015,1,2) },

                    //
                    new Booking { RoomId = "3", StartTime = new DateTime(2015,1,1), EndTime = new DateTime(2015,1,1,11,0,0) },
                    new Booking { RoomId = "3", StartTime = new DateTime(2015,1,1,11,0,0), EndTime = new DateTime(2015,1,2) },

                    // Should return 9-9:30
                    new Booking { RoomId = "4", StartTime = new DateTime(2015,1,1), EndTime = new DateTime(2015,1,1,9,0,0) },
                    new Booking { RoomId = "4", StartTime = new DateTime(2015,1,1,9,30,0), EndTime = new DateTime(2015,1,1,10,0,0) },
                    new Booking { RoomId = "4", StartTime = new DateTime(2015,1,1,10,0,0), EndTime = new DateTime(2015,1,2) },

                    // 2 available slots (10:00 - 10:30 and 11 - 12). Should return 10-10:30
                    new Booking { RoomId = "6", StartTime = new DateTime(2015,1,1), EndTime = new DateTime(2015,1,1,10,0,0) },
                    new Booking { RoomId = "6", StartTime = new DateTime(2015,1,1,10,30,0), EndTime = new DateTime(2015,1,1,11,0,0) },
                    new Booking { RoomId = "6", StartTime = new DateTime(2015,1,1,12,0,0), EndTime = new DateTime(2015,1,2) }

                }.AsRepository();

                var target = new RoomSearchController
                {
                    RoomRepository = rooms,
                    RoomBookingRepository = bookings
                };

                var result = target.Get(new DateTime(2015, 1, 1, 9, 0, 0), new DateTime(2015, 1, 2), 2).ToArray();

                result.ShouldBeEquivalentTo(new[]
                {
                    new RoomSearchResult { Name = "Room 2", Start = new DateTime(2015, 1, 1, 9, 0, 0), End = new DateTime(2015, 1, 1, 10, 0, 0) },
                    new RoomSearchResult { Name = "Room 4", Start = new DateTime(2015, 1, 1, 9, 0, 0), End = new DateTime(2015, 1, 1, 9, 30, 0) },
                    new RoomSearchResult { Name = "Room 5", Start = new DateTime(2015, 1, 1, 9, 0, 0), End = new DateTime(2015, 1, 2) },
                    new RoomSearchResult { Name = "Room 6", Start = new DateTime(2015, 1, 1, 10, 0, 0), End = new DateTime(2015, 1, 1, 10, 30, 0) }
                });
            }
        }
    }
}
