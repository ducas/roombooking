﻿using System;
using System.Web.Http;
using FluentAssertions;
using NUnit.Framework;
using RoomBooking.Controllers.Api;
using RoomBooking.Models.Api;
using Booking = RoomBooking.Data.RoomBooking;
using Room = RoomBooking.Data.Room;

namespace RoomBooking.Tests.Controllers.Api.RoomBookings
{
    public class GetList
    {
        public static readonly DateTime DefaultDate = TimeZoneInfo.ConvertTimeToUtc(new DateTime(2015, 1, 1));

        public class should_throw_exception
        {
            [Test]
            public void when_room_not_found()
            {
                var rooms = new[] { new Room { Name = "Room 1" } }.AsRepository();
                var bookings = new Booking[] { }.AsRepository();

                var request = new FakeHttpRequestMessage();

                var target = new RoomBookingsController { RoomRepository = rooms, RoomBookingRepository = bookings, Request = request };

                Action action = () => target.Get(new GetRoomBookingsRequest { RoomName = "Room 2", Year = 2015, Month = 1, Day = 1 });
                action.ShouldThrow<HttpResponseException>();
            }
        }

        public class should_return_empty
        {
            [Test]
            public void when_no_bookings_exist()
            {
                var rooms = new[] { new Room { Id = "room1", Name = "Room 1" } }.AsRepository();
                var bookings = new Booking[] { }.AsRepository();

                var target = new RoomBookingsController { RoomRepository = rooms, RoomBookingRepository = bookings };

                var model = new GetRoomBookingsRequest { RoomName = "Room 1", Year = 2015, Month = 1, Day = 1 };
                var result = target.Get(model);
                result.Should().BeEmpty();
            }
            [Test]
            public void when_bookings_exist_for_different_room()
            {
                var date = DefaultDate;

                var rooms = new[] { new Room { Id = "room1", Name = "Room 1" } }.AsRepository();
                var bookings = new[] { new Booking { RoomId = "room2", StartTime = date.AddHours(9), EndTime = date.AddHours(10) } }.AsRepository();

                var target = new RoomBookingsController { RoomRepository = rooms, RoomBookingRepository = bookings };

                var model = new GetRoomBookingsRequest { RoomName = "Room 1", Year = 2015, Month = 1, Day = 1 };
                var result = target.Get(model);
                result.Should().BeEmpty();
            }

            [Test]
            public void when_bookings_exist_for_room_on_different_day()
            {
                var date = DefaultDate.AddDays(1);

                var rooms = new[] { new Room { Id = "room1", Name = "Room 1" } }.AsRepository();
                var bookings = new[] { new Booking { RoomId = "room1", StartTime = date.AddHours(9), EndTime = date.AddHours(10) } }.AsRepository();

                var target = new RoomBookingsController { RoomRepository = rooms, RoomBookingRepository = bookings };

                var model = new GetRoomBookingsRequest { RoomName = "Room 1", Year = 2015, Month = 1, Day = 1 };
                var result = target.Get(model);
                result.Should().BeEmpty();
            }

            [Test]
            public void when_bookings_exist_for_different_room_encapsulating_day()
            {
                var date = DefaultDate;

                var rooms = new[] { new Room { Id = "room1", Name = "Room 1" }, new Room { Id = "room2", Name = "Room 2" } }.AsRepository();
                var bookings = new[] { new Booking { RoomId = "room1", StartTime = date.AddHours(-1), EndTime = date.AddHours(25) } }.AsRepository();

                var target = new RoomBookingsController { RoomRepository = rooms, RoomBookingRepository = bookings };

                var model = new GetRoomBookingsRequest { RoomName = "Room 2", Year = 2015, Month = 1, Day = 2 };
                var result = target.Get(model);
                result.Should().BeEmpty();
            }
        }

        public class should_return_bookings
        {
            [Test]
            public void when_bookings_exist_for_room_on_day()
            {
                var date = new DateTime(2015, 1, 1);

                var rooms = new[] { new Room { Id = "room1", Name = "Room 1" } }.AsRepository();
                var bookings = new[] { new Booking { RoomId = "room1", StartTime = date.AddHours(9), EndTime = date.AddHours(10) } }.AsRepository();

                var target = new RoomBookingsController { RoomRepository = rooms, RoomBookingRepository = bookings };

                var model = new GetRoomBookingsRequest { RoomName = "Room 1", Year = 2015, Month = 1, Day = 1 };
                var result = target.Get(model);
                result.Should().NotBeEmpty();
            }

            [Test]
            public void when_bookings_exist_for_room_starting_on_day()
            {
                var date = TimeZoneInfo.ConvertTimeToUtc(new DateTime(2015, 1, 1));

                var rooms = new[] { new Room { Id = "room1", Name = "Room 1" } }.AsRepository();
                var bookings = new[] { new Booking { RoomId = "room1", StartTime = date.AddHours(23), EndTime = date.AddHours(25) } }.AsRepository();

                var target = new RoomBookingsController { RoomRepository = rooms, RoomBookingRepository = bookings };

                var model = new GetRoomBookingsRequest { RoomName = "Room 1", Year = 2015, Month = 1, Day = 1 };
                var result = target.Get(model);
                result.Should().NotBeEmpty();
            }

            [Test]
            public void when_bookings_exist_for_room_finishing_on_day()
            {
                var date = new DateTime(2015, 1, 1);

                var rooms = new[] { new Room { Id = "room1", Name = "Room 1" } }.AsRepository();
                var bookings = new[] { new Booking { RoomId = "room1", StartTime = date.AddHours(-1), EndTime = date.AddHours(1) } }.AsRepository();

                var target = new RoomBookingsController { RoomRepository = rooms, RoomBookingRepository = bookings };

                var model = new GetRoomBookingsRequest { RoomName = "Room 1", Year = 2015, Month = 1, Day = 1 };
                var result = target.Get(model);
                result.Should().NotBeEmpty();
            }

            [Test]
            public void when_bookings_exist_for_room_encapsulating_day()
            {
                var date = new DateTime(2015, 1, 1);

                var rooms = new[] { new Room { Id = "room1", Name = "Room 1" } }.AsRepository();
                var bookings = new[] { new Booking { RoomId = "room1", StartTime = date.AddHours(-1), EndTime = date.AddHours(25) } }.AsRepository();

                var target = new RoomBookingsController { RoomRepository = rooms, RoomBookingRepository = bookings };

                var model = new GetRoomBookingsRequest { RoomName = "Room 1", Year = 2015, Month = 1, Day = 1 };
                var result = target.Get(model);
                result.Should().NotBeEmpty();
            }
        }
    }
}
