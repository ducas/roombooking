﻿using System;
using FluentAssertions;
using NUnit.Framework;
using RoomBooking.Controllers.Api;
using RoomBooking.Models.Api;
using Room = RoomBooking.Data.Room;
using Booking = RoomBooking.Data.RoomBooking;

namespace RoomBooking.Tests.Controllers.Api.RoomBookings
{
    public class Post
    {
        private static CreateRoomBookingRequest BodyModel()
        {
            return new CreateRoomBookingRequest
            {
                AttendeeCount = 10,
                Duration = 60,
                MeetingName = "My Meeting",
                OrganiserName = "me",
                StartTime = new DateTime(2015, 1, 1, 9, 0, 0)
            };
        }

        private static GetRoomBookingsRequest UrlModel()
        {
            return new GetRoomBookingsRequest { RoomName = "Room 1", Year = 2015, Month = 1, Day = 1 };
        }

        public class should_return_error
        {
            [Test]
            public void when_model_invalid()
            {
                var rooms = new[] { new Room { Name = "Room 1", Capacity = 10 } }.AsRepository();
                var bookings = new Booking[] {}.AsRepository();

                var request = new FakeHttpRequestMessage();

                var target = new RoomBookingsController { RoomRepository = rooms, RoomBookingRepository = bookings, Request = request };
                target.ModelState.AddModelError("test", "error");

                var result = target.Post(UrlModel(), BodyModel());
                result.IsSuccessStatusCode.Should().BeFalse();
            }

            [Test]
            public void when_room_not_found()
            {
                var rooms = new[] { new Room { Name = "Room 2", Capacity = 10 } }.AsRepository();
                var bookings = new Booking[] { }.AsRepository();

                var request = new FakeHttpRequestMessage();

                var target = new RoomBookingsController { RoomRepository = rooms, RoomBookingRepository = bookings, Request = request };

                var result = target.Post(UrlModel(), BodyModel());
                result.IsSuccessStatusCode.Should().BeFalse();
            }

            [Test]
            public void when_above_room_capacity()
            {
                var rooms = new[] { new Room { Name = "Room 1", Capacity = 1 } }.AsRepository();
                var bookings = new Booking[] { }.AsRepository();

                var request = new FakeHttpRequestMessage();

                var target = new RoomBookingsController { RoomRepository = rooms, RoomBookingRepository = bookings, Request = request };

                var result = target.Post(UrlModel(), BodyModel());
                result.IsSuccessStatusCode.Should().BeFalse();
            }

            [Test]
            public void when_room_has_conflicting_booking()
            {
                var urlModel = UrlModel();
                var bodyModel = BodyModel();
                var startTime = bodyModel.StartTime;

                var rooms = new[] { new Room { Name = "Room 1", Capacity = 10 } }.AsRepository();
                var bookings = new[] { new Booking { StartTime = startTime, EndTime = startTime.AddMinutes(30) } }.AsRepository();

                var request = new FakeHttpRequestMessage();

                var target = new RoomBookingsController { RoomRepository = rooms, RoomBookingRepository = bookings, Request = request };

                var result = target.Post(urlModel, bodyModel);
                result.IsSuccessStatusCode.Should().BeFalse();
            }
        }

        public class should_return_success
        {
            [Test]
            public void when_booking_successfully_created()
            {
                var urlModel = UrlModel();
                var bodyModel = BodyModel();
                var startTime = bodyModel.StartTime;

                var rooms = new[] { new Room { Name = "Room 1", Capacity = 10 } }.AsRepository();
                var bookings = new[] { new Booking { StartTime = startTime.AddHours(-1), EndTime = startTime } }.AsRepository();

                var request = new FakeHttpRequestMessage();

                var target = new RoomBookingsController { RoomRepository = rooms, RoomBookingRepository = bookings, Request = request };

                var result = target.Post(urlModel, bodyModel);
                result.IsSuccessStatusCode.Should().BeTrue();
            }
        }
    }
}
