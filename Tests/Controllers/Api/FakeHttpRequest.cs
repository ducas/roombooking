﻿using System.Net.Http;
using System.Web.Http;

namespace RoomBooking.Tests.Controllers.Api
{
    public class FakeHttpRequestMessage : HttpRequestMessage
    {
        public FakeHttpRequestMessage()
        {
            this.SetConfiguration(new HttpConfiguration());
        }
    }
}
