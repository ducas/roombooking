﻿using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using MongoRepository;
using NSubstitute;
using NUnit.Framework;
using RoomBooking.Controllers.Api;
using RoomBooking.Data;

namespace RoomBooking.Tests.Controllers.Api.Rooms
{
    public class GetList
    {
        [Test]
        public void should_return_all_rooms()
        {

            var data = new List<Room>
            {
                new Room {Name = "Room 1", Capacity = 1},
                new Room {Name = "Room 2", Capacity = 2}
            };

            var repository = data.AsRepository();

            var target = new RoomsController { RoomRepository = repository };
            var result = target.Get();

            result.ShouldAllBeEquivalentTo(data);
        }
    }
}
