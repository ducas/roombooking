# Deploy

## Azure Website

This solution is set up to be deployed to an azure website whenever a commit is made to the master branch, so commit wisely...

Website - http://ducas-roombooking.azurewebsites.net.

It connects to a MongoDB database hosted on [MongoLab](https://mongolab.com). The connection string is configured in the Azure Website's configuration page. To run the application locally, [Download MongoDB](https://www.mongodb.org/downloads), [start it up](http://docs.mongodb.org/manual/tutorial/manage-mongodb-processes/) (ensuring you've created the data directory) and hit F5.

## Creating a new environment

### Setup MongoDB

1. [Download MongoDB](https://www.mongodb.org/downloads)
2. Install it...
3. Create the data directory (e.g. C:\data\db on Windows)
4. Run mongod.exe
5. If deploying the web application to a different server, open port 27017 on the Windows firewall.

### Deploy the website

1. Run up a Windows machine and install IIS and .NET 4.5
2. Open IIS and create a new website (e.g. roombookings.mydomain.com)
3. Open the RoomBooking.sln in Visual Studio and publish the Web project to a local folder (e.g. C:\Temp\RoomBooking)
4. Copy the output from the local folder (e.g. C:\Temp\RoomBooking) to the IIS website root.
5. If MongoDB is on a different machine, change the connectionString named MongoServerSettings to point to the appropriate server