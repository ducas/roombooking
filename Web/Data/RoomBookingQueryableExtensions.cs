using System;
using System.Collections.Generic;
using System.Linq;
using MongoRepository;

namespace RoomBooking.Data
{
    public static class RoomBookingQueryableExtensions
    {
        public static IQueryable<Data.RoomBooking> FindMatchingBookings(this IRepository<Data.RoomBooking> repository, Room room, DateTime @from, DateTime to)
        {
            return repository.Where(b =>
                b.RoomId == room.Id
                && (
                    (@from <= b.StartTime && b.StartTime < to)
                    || (@from < b.EndTime && b.EndTime <= to)
                    || (b.StartTime < @from && to <= b.EndTime)
                    ))
                .OrderBy(b => b.StartTime);
        }

        public static IEnumerable<Data.RoomBooking> AvailableSlots(this IQueryable<Data.RoomBooking> roomBookings, DateTime start, DateTime end)
        {
            var items = roomBookings == null ? null : roomBookings.Where(b => b.EndTime >= start).ToArray();

            if (items == null || !items.Any())
            {
                yield return new RoomBooking { StartTime = start, EndTime = end };
                yield break;
            }

            var currentStart = start;
            for (var i = 0; i < items.Length; i++)
            {
                var current = items[i];
                if (current.StartTime > currentStart) yield return new RoomBooking { StartTime = currentStart, EndTime = current.StartTime };

                if (i == items.Length - 1)
                {
                    if (current.EndTime < end) yield return new RoomBooking { StartTime = current.EndTime, EndTime = end };
                    break;
                } // at the end

                currentStart = current.EndTime;
            }

        }
    }
}