﻿using MongoRepository;

namespace RoomBooking.Data
{
    public class Room : Entity
    {
        public int Capacity { get; set; }
        public string Name { get; set; }
    }
}
