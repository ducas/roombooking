﻿using System;
using MongoRepository;

namespace RoomBooking.Data
{
    public class RoomBooking : Entity
    {
        public string RoomId { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public int AttendeeCount { get; set; }
        public string OrganiserName { get; set; }
        public string MeetingName { get; set; }
    }
}
