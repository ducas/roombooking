﻿using Autofac;
using MongoRepository;
using RoomBooking.Data;

namespace RoomBooking.Core
{
    public class Module : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(c => new MongoRepository<Room>())
                .As<IRepository<Room>>()
                .InstancePerRequest();
            builder.Register(c => new MongoRepository<Data.RoomBooking>())
                .As<IRepository<Data.RoomBooking>>()
                .InstancePerRequest();
        }
    }
}
