﻿namespace RoomBooking.Models.Api
{
    public class GetRoomBookingsRequest
    {
        public string RoomName { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public int Day { get; set; }
    }
}
