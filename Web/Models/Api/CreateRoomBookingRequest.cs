﻿using System;
using System.ComponentModel.DataAnnotations;

namespace RoomBooking.Models.Api
{
    public class CreateRoomBookingRequest
    {
        [Required]
        public DateTime StartTime { get; set; }
        [Required]
        public int AttendeeCount { get; set; }
        [Required]
        public int Duration { get; set; }
        public string OrganiserName { get; set; }
        public string MeetingName { get; set; }
    }
}
