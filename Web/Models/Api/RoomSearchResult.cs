using System;

namespace RoomBooking.Models.Api
{
    public class RoomSearchResult
    {
        public string Name { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
    }
}