﻿namespace RoomBooking.Models.Api
{
    public class Room
    {
        public int Capacity { get; set; }
        public string Name { get; set; }
    }
}
