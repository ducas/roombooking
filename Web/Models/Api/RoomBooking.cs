﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoomBooking.Models.Api
{
    public class RoomBooking
    {
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public int Duration { get; set; }
        public int AttendeeCount { get; set; }
        public string OrganiserName { get; set; }
        public string MeetingName { get; set; }
    }
}
