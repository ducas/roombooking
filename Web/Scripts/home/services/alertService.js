﻿(function (ng) {

    ng.module('roomBookingServices')
        .factory('alertService', [
            '$rootScope',
            function($rootScope) {

                var alerts = {
                    publish: function(alert) {
                        $rootScope.$broadcast('alertPublished', alert);
                    },
                    subscribe: function(handler) {
                        $rootScope.$on('alertPublished', function(e, alert) { handler(alert) });
                    }
                };

                return alerts;
            }
        ]);

})(angular);