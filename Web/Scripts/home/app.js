﻿(function (ng) {
    var roomBookingApp = ng.module('roomBookingApp', [
        'ngRoute',
        'ngSanitize',
        'ngMessages',
        'roomBookingServices',
        'roomBookingControllers'
    ]);

    ng.module('roomBookingControllers', []);
    ng.module('roomBookingServices', []);

    roomBookingApp.config(['$routeProvider',
        function ($routeProvider) {
            $routeProvider
                .when('/rooms', {
                    templateUrl: '/scripts/home/views/rooms.html',
                    controller: 'RoomListController'
                })
                .when('/rooms/availability/:roomName', {
                    templateUrl: '/scripts/home/views/availability.html',
                    controller: 'RoomAvailabilityController'
                })
                .when('/rooms/book/:roomName/:date', {
                    templateUrl: '/scripts/home/views/booking.html',
                    controller: 'RoomBookingController'
                })
                .when('/rooms/search', {
                    templateUrl: '/scripts/home/views/search.html',
                    controller: 'RoomSearchController'
                })
                .otherwise({
                    redirectTo: '/rooms'
                });
        }
    ]);
})(angular);
