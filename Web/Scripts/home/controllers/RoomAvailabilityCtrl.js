﻿(function (ng) {

    ng.module('roomBookingControllers')
        .controller('RoomAvailabilityController', [
            '$scope', '$http', '$routeParams',
            function($scope, $http, $routeParams) {
                $scope.roomName = $routeParams.roomName;
                $scope.date = new Date();

                $scope.loadAvailability = function() {
                    var date = $scope.date;

                    if (!date) {
                        return;
                    }

                    var url = '/api/rooms/' + $scope.roomName + '/bookings/' + moment(date).format('YYYY/MM/DD');
                    $http.get(url).success(function(data) {
                        $scope.bookings = data;
                    });
                };

                $scope.loadAvailability();
            }
        ]);

})(angular);