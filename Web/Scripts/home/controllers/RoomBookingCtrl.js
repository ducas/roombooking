﻿(function (ng) {
    
    ng.module('roomBookingControllers')
        .controller('RoomBookingController', [
            '$scope', '$http', '$routeParams', '$location', 'alertService',
            function ($scope, $http, $routeParams, $location, alertService) {
                var i;

                $http.get('/api/rooms').success(function (data) {
                    $scope.rooms = data.map(function (r) { return r.Name; });
                    $scope.roomName = $routeParams.roomName;
                });

                $scope.hours = [];
                for (i = 1; i < 13; i++) {
                    $scope.hours.push(i);
                }
                $scope.minutes = [];
                for (i = 0; i < 60; i++) {
                    $scope.minutes.push(('0' + i).slice(-2));
                }

                $scope.startDate = new Date($routeParams.date);
                $scope.startHour = 9;
                $scope.startMinute = '00';
                $scope.startMeridiem = 'AM';
                $scope.duration = 30;
                $scope.attendeeCount = 1;
                $scope.organiserName = '';
                $scope.meetingName = '';
                $scope.errors = {};

                $scope.submit = function () {
                    $scope.errors.length = 0;
                    var date = $scope.startDate;
                    var url = '/api/rooms/' + $scope.roomName + '/bookings/' + moment(date).format('YYYY/MM/DD');
                    var startHour = $scope.startHour + ($scope.startMeridiem === 'PM' ? 12 : 0);
                    $http.post(url,
                        {
                            startTime: moment(date).hours(startHour).minutes($scope.startMinute).toISOString(),
                            duration: $scope.duration,
                            attendeeCount: $scope.attendeeCount,
                            organiserName: $scope.organiserName,
                            meetingName: $scope.meetingName
                        })
                        .success(function () {
                            $location.path('#');
                            alertService.publish({ type: 'success', message: '<strong>Woohoo!</strong> Your room is booked and you\'re all set to go...' });
                        })
                        .error(function (data) {
                            if (!!data && !!data.ModelState) {
                                for (var property in data.ModelState) {
                                    if (data.ModelState.hasOwnProperty(property)) {
                                        $scope.errors[property] = data.ModelState[property];
                                    }
                                }
                            }
                            alertService.publish({ type: 'warning', message: '<strong>Oh snap!</strong> Something went wrong booking your room...' });
                        });
                };
            }
        ]);

})(angular);