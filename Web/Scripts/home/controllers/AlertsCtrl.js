﻿(function (ng) {

    ng.module('roomBookingControllers')
        .controller('AlertsController', [
            '$scope', 'alertService',
            function ($scope, alertService) {
                $scope.alerts = [];

                alertService.subscribe(function (alert) {
                    $scope.alerts.push(alert);
                });

                $scope.removeAlert = function (alert) {
                    $scope.alerts.splice($scope.alerts.indexOf(alert), 1);
                };
            }
        ]);

})(angular);