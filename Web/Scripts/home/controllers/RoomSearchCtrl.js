﻿(function (ng) {

    ng.module('roomBookingControllers')
        .controller('RoomSearchController', [
            '$scope', '$http',
            function ($scope, $http) {
                var i;

                $scope.hours = [];
                for (i = 1; i < 13; i++) {
                    $scope.hours.push(i);
                }
                $scope.minutes = [];
                for (i = 0; i < 60; i++) {
                    $scope.minutes.push(('0' + i).slice(-2));
                }

                $scope.startDate = new Date();
                $scope.startHour = 9;
                $scope.startMinute = '00';
                $scope.startMeridiem = 'AM';
                $scope.attendeeCount = 1;

                $scope.showResults = false;
                $scope.rooms = [];

                $scope.submit = function () {
                    var date = moment($scope.startDate)
                        .hour($scope.startHour + ($scope.startMeridiem === 'PM' ? 12 : 0))
                        .minute($scope.startMinute);

                    $http.get('/api/rooms/search', {
                            params: {
                                start: date.toISOString(),
                                end: date.startOf('day').add(1, 'd').toISOString(),
                                attendeeCount: $scope.attendeeCount
                            }
                        })
                        .success(function (data) {
                            $scope.showResults = false;
                            $scope.rooms.length = 0;
                            if (!!data) {
                                data.forEach(function (r) { $scope.rooms.push(r); });
                            }
                            $scope.showResults = true;
                        });
                };

            }
        ]);

})(angular);