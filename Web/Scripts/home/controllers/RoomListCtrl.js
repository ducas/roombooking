﻿(function (ng) {

    ng.module('roomBookingControllers')
        .controller('RoomListController', [
            '$scope', '$http', '$location',
            function ($scope, $http, $location) {
                $http.get('/api/rooms').success(function (data) {
                    $scope.rooms = data;
                });

                $scope.selectRoom = function (room) {
                    $location.path('/rooms/availability/' + room.Name);
                };
            }
        ]);

})(angular);