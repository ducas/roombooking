﻿/// <reference path="../../../moment.js" />
/// <reference path="../../../angular.js" />
/// <reference path="../../../angular-route.js" />
/// <reference path="../../../angular-mocks.js" />
/// <reference path="../../app.js" />
/// <reference path="../../services/alertService.js" />

// ReSharper disable UseOfImplicitGlobalInFunctionScope
describe('alertService', function () {

    beforeEach(module('roomBookingServices'));

    var mock, alertService;
    beforeEach(function () {
        mock = { $broadcast: jasmine.createSpy(), $on: jasmine.createSpy() };

        module(function ($provide) {
            $provide.value('$rootScope', mock);
        });

        inject(function ($injector) {
            alertService = $injector.get('alertService');
        });
    });

    it('should broadcast alert when received', function () {
        var alert = {};
        alertService.publish(alert);
        expect(mock.$broadcast).toHaveBeenCalledWith('alertPublished', alert);
    });

    it('should call handler when alert broadcasted', function () {
        var alert = {},
            args = {},
            handler = function () { args = arguments; };

        alertService.subscribe(handler);

        expect(mock.$on).toHaveBeenCalledWith('alertPublished', jasmine.any(Function));

        var callback = mock.$on.calls.mostRecent().args[1];
        callback({}, alert);

        expect(args).toBeDefined(alert);
        expect(args[0]).toBe(alert);
    });
});
// ReSharper restore UseOfImplicitGlobalInFunctionScope
