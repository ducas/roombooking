﻿/// <reference path="../../../moment.js" />
/// <reference path="../../../angular.js" />
/// <reference path="../../../angular-route.js" />
/// <reference path="../../../angular-sanitize.js" />
/// <reference path="../../../angular-messages.js" />
/// <reference path="../../../angular-mocks.js" />
/// <reference path="../../app.js" />
/// <reference path="../../controllers/AlertsCtrl.js" />
/// <reference path="../../services/alertService.js" />

// ReSharper disable UseOfImplicitGlobalInFunctionScope
describe('AlertsController', function () {

    beforeEach(module('roomBookingApp'));
    beforeEach(module('roomBookingServices'));
    beforeEach(module('roomBookingControllers'));

    var scope, ctrl, alertService;

    beforeEach(function () {
        alertService = { handlers: [], subscribe: function (handler) { alertService.handlers.push(handler); } };
    });

    it('should subscribe to alerts when initialising', inject(function ($rootScope, $controller) {
        spyOn(alertService, 'subscribe');
        scope = $rootScope.$new();
        ctrl = $controller('AlertsController', { $scope: scope, alertService: alertService });
        expect(alertService.subscribe).toHaveBeenCalled();
    }));

    describe('when initialised', function () {

        beforeEach(inject(function ($rootScope, $controller) {
            scope = $rootScope.$new();
            ctrl = $controller('AlertsController', { $scope: scope, alertService: alertService });
        }));

        it('should add to alerts when alert published', function () {
            var alert1 = {}, alert2 = {};
            alertService.handlers.forEach(function (h) { h(alert1); });
            alertService.handlers.forEach(function (h) { h(alert2); });
            expect(scope.alerts.length).toBe(2);
            expect(scope.alerts[0]).toBe(alert1);
            expect(scope.alerts[1]).toBe(alert2);
        });

        it('should remove alert when closed', function () {
            var alert1 = {}, alert2 = {};
            alertService.handlers.forEach(function (h) { h(alert1); });
            alertService.handlers.forEach(function (h) { h(alert2); });
            scope.removeAlert(alert1);
            expect(scope.alerts.length).toBe(1);
            expect(scope.alerts[0]).toBe(alert2);
        });
    });

});
// ReSharper restore UseOfImplicitGlobalInFunctionScope
