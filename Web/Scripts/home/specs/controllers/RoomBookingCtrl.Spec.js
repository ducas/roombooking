﻿/// <reference path="../../../moment.js" />
/// <reference path="../../../angular.js" />
/// <reference path="../../../angular-route.js" />
/// <reference path="../../../angular-sanitize.js" />
/// <reference path="../../../angular-messages.js" />
/// <reference path="../../../angular-mocks.js" />
/// <reference path="../../app.js" />
/// <reference path="../../controllers/RoomBookingCtrl.js" />
/// <reference path="../../services/alertService.js" />

// ReSharper disable UseOfImplicitGlobalInFunctionScope
describe('RoomBookingController', function () {

    beforeEach(module('roomBookingApp'));
    beforeEach(module('roomBookingServices'));
    beforeEach(module('roomBookingControllers'));

    var now,
        roomName,
        date,
        scope,
        ctrl,
        $httpBackend,
        routeParams,
        location,
        alertService,
        mockGetRequest = function (url, data) {
            $httpBackend.expectGET(url).respond(data);
        };

    beforeEach(inject(function (_$httpBackend_, $rootScope, $controller) {

        now = new Date();
        roomName = 'ROOMNAME';
        date = '2015-01-01';
        routeParams = { roomName: roomName, date: date };
        location = { path: jasmine.createSpy() };
        alertService = { publish: jasmine.createSpy() };

        $httpBackend = _$httpBackend_;
        mockGetRequest('/api/rooms', [{ Name: 'Room 1', Capacity: 10 }, { Name: 'Room 2', Capacity: 5 }]);

        scope = $rootScope.$new();
        ctrl = $controller('RoomBookingController', { $scope: scope, $routeParams: routeParams, $location: location, alertService: alertService });
    }));

    it('should create "rooms" model with 2 bookings fetched from xhr', function () {
        $httpBackend.flush();
        expect(scope.rooms.length).toBe(2);
    });

    it('should set "startDate" and "roomName" on scope from route params', function () {
        $httpBackend.flush();
        expect(scope.roomName).toBe(roomName);
        expect(moment(scope.startDate).format('YYYY-MM-DD')).toBe(date);
    });

    it('should populate "hours" model', function () {
        expect(scope.hours.length).toBe(12);
    });

    it('should populate "minutes" model', function () {
        expect(scope.minutes.length).toBe(60);
    });

    it('should set default model values', function () {
        expect(scope.startHour).toBe(9);
        expect(scope.startMinute).toBe('00');
        expect(scope.startMeridiem).toBe('AM');
        expect(scope.duration).toBe(30);
        expect(scope.attendeeCount).toBe(1);
        expect(scope.organiserName).toBe('');
        expect(scope.meetingName).toBe('');
    });

    it('should post data to API when user submits form', function () {
        $httpBackend.flush();

        $httpBackend.expectPOST('/api/rooms/ROOMNAME/bookings/2015/01/01', {
            startTime: moment('2015-01-01T09:00').toISOString(),
            duration: 30,
            attendeeCount: 1,
            organiserName: '',
            meetingName: ''
        }).respond({});

        scope.submit();

        $httpBackend.verifyNoOutstandingExpectation();
    });

    it('should post modified data to API when values changed before user submits form', function () {
        $httpBackend.flush();

        scope.roomName = 'ROOM2';
        scope.startDate = new Date('2015-01-03');
        scope.startHour = 10;
        scope.startMinute = '30';
        scope.startMeridiem = 'PM';
        scope.duration = 60;
        scope.attendeeCount = 2;
        scope.organiserName = 'me';
        scope.meetingName = 'my';

        $httpBackend.expectPOST('/api/rooms/ROOM2/bookings/2015/01/03', {
            startTime: moment('2015-01-03T22:30').toISOString(),
            duration: 60,
            attendeeCount: 2,
            organiserName: 'me',
            meetingName: 'my'
        }).respond({});

        scope.submit();

        $httpBackend.verifyNoOutstandingExpectation();
    });

    it('should take user to home and display success message when room successfully booked', function () {
        $httpBackend.flush();

        $httpBackend.expectPOST('/api/rooms/ROOMNAME/bookings/2015/01/01', {
            startTime: moment('2015-01-01T09:00').toISOString(),
            duration: 30,
            attendeeCount: 1,
            organiserName: '',
            meetingName: ''
        }).respond({});

        scope.submit();
        $httpBackend.flush();

        expect(location.path).toHaveBeenCalledWith('#');
        expect(alertService.publish).toHaveBeenCalled();

        expect(alertService.publish.calls.mostRecent().args[0].type).toBe('success');
    });

    it('should stay on page and display warning message when room not successfully booked', function () {
        $httpBackend.flush();

        $httpBackend.expectPOST('/api/rooms/ROOMNAME/bookings/2015/01/01', {
            startTime: moment('2015-01-01T09:00').toISOString(),
            duration: 30,
            attendeeCount: 1,
            organiserName: '',
            meetingName: ''
        }).respond(400, '');

        scope.submit();
        $httpBackend.flush();

        expect(location.path).not.toHaveBeenCalled();
        expect(alertService.publish).toHaveBeenCalled();

        expect(alertService.publish.calls.mostRecent().args[0].type).toBe('warning');
    });
});
// ReSharper restore UseOfImplicitGlobalInFunctionScope

