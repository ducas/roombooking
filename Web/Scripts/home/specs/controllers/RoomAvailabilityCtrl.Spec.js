﻿/// <reference path="../../../moment.js" />
/// <reference path="../../../angular.js" />
/// <reference path="../../../angular-route.js" />
/// <reference path="../../../angular-sanitize.js" />
/// <reference path="../../../angular-messages.js" />
/// <reference path="../../../angular-mocks.js" />
/// <reference path="../../app.js" />
/// <reference path="../../controllers/RoomAvailabilityCtrl.js" />

// ReSharper disable UseOfImplicitGlobalInFunctionScope
describe('RoomAvailabilityController', function () {

    beforeEach(module('roomBookingApp'));
    beforeEach(module('roomBookingServices'));
    beforeEach(module('roomBookingControllers'));

    var now = new Date(),
        roomName = 'ROOMNAME',
        scope,
        ctrl,
        $httpBackend,
        routeParams = { roomName: roomName },
        buildUrl = function (date) {
            return '/api/rooms/' + roomName + '/bookings/' + moment(date).format('YYYY/MM/DD');
        },
        mockRequest = function (date, data) {
            $httpBackend.expectGET(buildUrl(date)).respond(data);
        };

    beforeEach(inject(function (_$httpBackend_, $rootScope, $controller) {
        $httpBackend = _$httpBackend_;
        mockRequest(now, [
            { StartTime: new Date('20150101T09:00:00.000Z'), EndDate: new Date('20150101T10:00:00.000Z'), Duration: 60 },
            { StartTime: new Date('20150101T10:00:00.000Z'), EndDate: new Date('20150101T11:00:00.000Z'), Duration: 60 }
        ]);

        scope = $rootScope.$new();
        ctrl = $controller('RoomAvailabilityController', { $scope: scope, $routeParams: routeParams });
    }));

    it('should create "bookings" model with 2 bookings fetched from xhr', function () {
        $httpBackend.flush();
        expect(scope.bookings.length).toBe(2);
    });

    it('should populate "bookings" model with 1 booking fetched from xhr when date changed', function () {
        $httpBackend.flush();
        expect(scope.bookings.length).toBe(2);
        var newDate = new Date('20140101');
        mockRequest(newDate, [{ StartTime: new Date('20150101T09:00:00.000Z'), EndDate: new Date('20150101T10:00:00.000Z'), Duration: 60 }]);
        scope.date = newDate;
        scope.loadAvailability();
        $httpBackend.flush();
        expect(scope.bookings.length).toBe(1);
    });

});
// ReSharper restore UseOfImplicitGlobalInFunctionScope
