﻿/// <reference path="../../../moment.js" />
/// <reference path="../../../angular.js" />
/// <reference path="../../../angular-route.js" />
/// <reference path="../../../angular-sanitize.js" />
/// <reference path="../../../angular-messages.js" />
/// <reference path="../../../angular-mocks.js" />
/// <reference path="../../app.js" />
/// <reference path="../../controllers/RoomSearchCtrl.js" />

// ReSharper disable UseOfImplicitGlobalInFunctionScope
describe('RoomSearchController', function () {

    beforeEach(module('roomBookingApp'));
    beforeEach(module('roomBookingServices'));
    beforeEach(module('roomBookingControllers'));

    var scope, ctrl, $httpBackend;

    beforeEach(inject(function (_$httpBackend_, $rootScope, $controller) {
        $httpBackend = _$httpBackend_;

        scope = $rootScope.$new();
        ctrl = $controller('RoomSearchController', { $scope: scope, $location: location });
    }));

    it('should set default values when initialising', function () {
        expect(moment(scope.startDate).format('YYYY-MM-dd')).toBe(moment(new Date()).format('YYYY-MM-dd'));
        expect(scope.startHour).toBe(9);
        expect(scope.startMinute).toBe('00');
        expect(scope.startMeridiem).toBe('AM');
        expect(scope.attendeeCount).toBe(1);
        expect(scope.showResults).toBe(false);
        expect(scope.rooms.length).toBe(0);
    });

    it('should search for rooms and populate results on form submit', function () {
        var date = moment(scope.startDate).hour(scope.startHour).minute(scope.startMinute);
        var start = date.toISOString();
        var end = date.startOf('day').add(1, 'd').toISOString();
        $httpBackend.expectGET('/api/rooms/search?attendeeCount=1&end=' + end + '&start=' + start).
            respond([{ Name: 'Room 1', Capacity: 10 }, { Name: 'Room 2', Capacity: 5 }]);

        scope.submit();

        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.flush();

        expect(scope.showResults).toBe(true);
        expect(scope.rooms.length).toBe(2);
    });

});
// ReSharper restore UseOfImplicitGlobalInFunctionScope
