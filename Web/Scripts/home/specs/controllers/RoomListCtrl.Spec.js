﻿/// <reference path="../../../moment.js" />
/// <reference path="../../../angular.js" />
/// <reference path="../../../angular-route.js" />
/// <reference path="../../../angular-sanitize.js" />
/// <reference path="../../../angular-messages.js" />
/// <reference path="../../../angular-mocks.js" />
/// <reference path="../../app.js" />
/// <reference path="../../controllers/RoomListCtrl.js" />

// ReSharper disable UseOfImplicitGlobalInFunctionScope
describe('RoomListController', function () {

    beforeEach(module('roomBookingApp'));
    beforeEach(module('roomBookingControllers'));

    var scope, ctrl, $httpBackend, location = { path: function () { } };

    beforeEach(inject(function (_$httpBackend_, $rootScope, $controller) {
        $httpBackend = _$httpBackend_;
        $httpBackend.expectGET('/api/rooms').
            respond([{ Name: 'Room 1', Capacity: 10 }, { Name: 'Room 2', Capacity: 5 }]);

        scope = $rootScope.$new();
        ctrl = $controller('RoomListController', { $scope: scope, $location: location });
        $httpBackend.flush();
    }));

    it('should create "rooms" model with 2 rooms fetched from xhr', function () {
        expect(scope.rooms.length).toBe(2);
    });

    it('should navigate to room when item selected', function () {
        spyOn(location, 'path');
        scope.selectRoom({ Name: 'roomName' });
        expect(location.path).toHaveBeenCalledWith('/rooms/availability/roomName');
    });

});

// ReSharper restore UseOfImplicitGlobalInFunctionScope
