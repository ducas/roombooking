﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MongoRepository;
using RoomBooking.Data;
using RoomBooking.Models.Api;

namespace RoomBooking.Controllers.Api
{
    [RoutePrefix("api/rooms")]
    public class RoomBookingsController : ApiController
    {
        private static readonly TimeZoneInfo DefaultTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["RoomBooking.DefaultTimeZoneId"]);

        public IRepository<Data.Room> RoomRepository { get; set; }
        public IRepository<Data.RoomBooking> RoomBookingRepository { get; set; }

        [Route("{roomName}/bookings/{year}/{month}/{day}", Name = "Api_Rooms_GetBookings")]
        [HttpGet]
        public IEnumerable<Models.Api.RoomBooking> Get([FromUri] GetRoomBookingsRequest model)
        {
            var room = RoomRepository.FirstOrDefault(r => r.Name.ToLower() == model.RoomName.ToLower());
            if (room == null) throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.NotFound, "Room not found."));

            var localDate = new DateTime(model.Year, model.Month, model.Day);

            var date = TimeZoneInfo.ConvertTimeToUtc(localDate, DefaultTimeZone);
            var datePlusOne = date.AddDays(1);

            return RoomBookingRepository.FindMatchingBookings(room, date, datePlusOne)
                .Select(b => new Models.Api.RoomBooking
                {
                    StartTime = b.StartTime,
                    AttendeeCount = b.AttendeeCount,
                    Duration = (int)(b.EndTime - b.StartTime).TotalMinutes,
                    EndTime = b.EndTime,
                    MeetingName = b.MeetingName,
                    OrganiserName = b.OrganiserName
                });
        }

        [Route("{roomName}/bookings/{year}/{month}/{day}", Name = "Api_Rooms_PostBooking")]
        [HttpPost]
        public HttpResponseMessage Post([FromUri] GetRoomBookingsRequest urlModel, [FromBody]CreateRoomBookingRequest bodyModel)
        {
            Data.Room room = null;
            if (!string.IsNullOrWhiteSpace(urlModel.RoomName))
            {
                room = RoomRepository.FirstOrDefault(r => r.Name.ToLower() == urlModel.RoomName.ToLower());
                if (room == null)
                    ModelState.AddModelError("RoomName", "Room not found.");
            }

            if (room != null && bodyModel.AttendeeCount > room.Capacity)
            {
                ModelState.AddModelError("AttendeeCount", "Must be less than room capacity.");
            }

            if (!ModelState.IsValid)
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);

            var startTime = bodyModel.StartTime;
            var endTime = startTime.AddMinutes(bodyModel.Duration);

            var matching = RoomBookingRepository.FindMatchingBookings(room, startTime, endTime);
            if (matching.Any())
            {
                ModelState.AddModelError("RoomName", "There is a conflicting booking for this room.");
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            var booking = new Data.RoomBooking
            {
                RoomId = room.Id,
                AttendeeCount = bodyModel.AttendeeCount,
                StartTime = startTime,
                EndTime = endTime,
                MeetingName = bodyModel.MeetingName,
                OrganiserName = bodyModel.OrganiserName
            };

            RoomBookingRepository.Add(booking);

            return Request.CreateResponse();
        }
    }
}
