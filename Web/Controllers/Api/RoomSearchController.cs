﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using MongoRepository;
using RoomBooking.Data;
using RoomBooking.Models.Api;
using Room = RoomBooking.Data.Room;

namespace RoomBooking.Controllers.Api
{
    public class RoomSearchController : ApiController
    {
        public IRepository<Room> RoomRepository { get; set; }
        public IRepository<Data.RoomBooking> RoomBookingRepository { get; set; }

        [Route("api/rooms/search")]
        public IEnumerable<RoomSearchResult> Get(DateTime start, DateTime end, int attendeeCount)
        {
            var rooms = RoomRepository.Where(r => r.Capacity >= attendeeCount).ToList();

            var slots = rooms.Select(
                r => RoomBookingRepository
                    .FindMatchingBookings(r, start.Date, end)
                    .AvailableSlots(start, end)
                    .Select(s => new RoomSearchResult { Name = r.Name, Start = s.StartTime, End = s.EndTime })
                    .FirstOrDefault()
                )
                .Where(s => s != null);

            return slots;
        }
    }
}