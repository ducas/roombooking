﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using MongoRepository;
using RoomBooking.Models.Api;

namespace RoomBooking.Controllers.Api
{
    public class RoomsController : ApiController
    {
        public IRepository<Data.Room> RoomRepository { get; set; }

        public IEnumerable<Room> Get()
        {
            return RoomRepository.Select(r => new Room { Name = r.Name, Capacity = r.Capacity });
        }
    }
}
