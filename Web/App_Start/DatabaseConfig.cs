﻿using System.Collections.Generic;
using System.Linq;
using MongoRepository;
using RoomBooking.Data;

namespace RoomBooking
{
    public static class DatabaseConfig
    {
        public static void Configure()
        {
            var rooms = new MongoRepository<Room>();

            var existing = rooms.ToList();
            var newRooms = new List<Room>();

            if (existing.All(r => r.Name != "Hunter Valley"))
                newRooms.Add(new Room { Name = "Hunter Valley", Capacity = 10 });
            if (existing.All(r => r.Name != "Swan Valley"))
                newRooms.Add(new Room { Name = "Swan Valley", Capacity = 6 });
            if (existing.All(r => r.Name != "Central West"))
                newRooms.Add(new Room { Name = "Central West", Capacity = 4 });

            if (newRooms.Any()) rooms.Add(newRooms);
        }
    }
}
