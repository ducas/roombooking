﻿using System;
using System.Linq;
using System.Reflection;
using System.Web.Http;
using System.Web.Mvc;
using Autofac;
using Autofac.Builder;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;

namespace RoomBooking
{
    public static class DependencyConfig
    {
        public static void Configure()
        {
            var config = GlobalConfiguration.Configuration;
            var builder = new ContainerBuilder();

            var assembly = typeof(MvcApplication).Assembly;

            builder.RegisterControllers(assembly).PropertiesAutowired();
            builder.RegisterFilterProvider();

            builder.RegisterApiControllers(assembly).PropertiesAutowired();
            builder.RegisterWebApiFilterProvider(config);

            builder.RegisterAssemblyModules(assembly);

            var container = builder.Build();

            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }
    }
}
